package pl.edu.pwsztar.domain.dto;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class FileDto {

    private final List<MovieDto> movies;

    private FileDto(Builder builder) {
        this.movies = builder.movies;
    }

    public static Builder builder() {
        return new Builder();
    }

    public List<MovieDto> getMovies() {
        return movies;
    }

    public static final class Builder {
        private List<MovieDto> movies;

        public Builder() {
        }

        public Builder movies(List<MovieDto> movies) {
            this.movies = movies;
            return this;
        }

        public FileDto build() {
            this.movies = Optional.ofNullable(movies).orElse(Collections.emptyList());
            return new FileDto(this);
        }
    }
}

package pl.edu.pwsztar.domain.files;

import pl.edu.pwsztar.domain.dto.CreatedFileDto;
import pl.edu.pwsztar.domain.dto.FileDto;

import java.io.IOException;

public interface TxtFileGenerator {
    CreatedFileDto toTxt(FileDto fileDto) throws IOException;
}
